public class Main {
    public static void main(String[] args) {

        Mediatheque maMediatheque = new Mediatheque();
        maMediatheque.setNomProprietaire("Alice");

        Film film1 = new Film("Le Seigneur des Anneaux", 
                              new StringBuffer("F001"), 9, "Peter Jackson", 2001);
        Film film2 = new Film("Inception", 
                              new StringBuffer("F002"), 10, "Christopher Nolan", 2010);

        maMediatheque.add(film1);
        maMediatheque.add(film2);

        Livre livre1 = new Livre("Le Petit Prince", new StringBuffer("L001"), 9, 
                                 "Antoine de Saint-Exupéry", "1943", 96, "AAAAAA");
        maMediatheque.add(livre1);

        Media media1 = new Media("Harry Potter à l'école des sorciers", 
                                 new StringBuffer("L002"), 8);
        maMediatheque.add(media1);

        System.out.println("Médiathèque d'Alice :");
        System.out.println(maMediatheque);

        Mediatheque copieMediatheque = new Mediatheque(maMediatheque);
        copieMediatheque.setNomProprietaire("Bob");

        Film film3 = new Film("Interstellar", 
                              new StringBuffer("F003"), 10, "Christopher Nolan", 2014);
        copieMediatheque.add(film3);

        System.out.println("Médiathèque d'Alice après copie :");
        System.out.println(maMediatheque);
        System.out.println("Copie de la médiathèque, appartenant à Bob :");
        System.out.println(copieMediatheque);

    }
}
